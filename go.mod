module pkgs-org-query

go 1.20

require (
	github.com/google/go-querystring v1.1.0
	gopkg.in/yaml.v3 v3.0.1
)
