package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/google/go-querystring/query"
	"gopkg.in/yaml.v3"
)

type queryFields struct {
	Query         string `url:"query"`
	SearchOn      string `url:"search_on,omitempty"`
	Architecture  string `url:"architecture,omitempty"`
	Distributions []int  `url:"distributions,omitempty"`
	Repositories  []int  `url:"repositories,omitempty"`
	Official      bool   `url:"official,omitempty"`
}

type pkgSingle struct {
	filename       string `json:"filename"`
	filenameSrc    string `json:"filename_src"`
	name           string `json:"name"`
	epoch          string `json:"epoch"`
	version        string `json:"version"`
	release        string `json:"release"`
	architecture   string `json:"architecture"`
	distributionID int    `json:"distribution_id"`
	repositoryID   int    `json:"repository_id"`
	official       bool   `json:"official"`
	urlBinary      string `json:"url_binary"`
	urlSource      string `json:"url_source"`
}

func importToken() *http.Cookie {
	yamlFile, err := ioutil.ReadFile("token.yaml")
	if err != nil {
		log.Printf("yamlFile.Get err   #%v ", err)
	}

	var accessCookie http.Cookie

	err = yaml.Unmarshal(yamlFile, &accessCookie)
	if err != nil {
		log.Fatalf("Unmarshal: %v", err)
	}

	log.Print(accessCookie)
	return &accessCookie
}

func main() {
	req, err := http.NewRequest(http.MethodGet, "https://api.pkgs.org/v1/search", nil)
	if err != nil {
		log.Print(err)
		os.Exit(1)
	}

	req.AddCookie(importToken())

	opts := queryFields{
		Query: "hebcal",
		//SearchOn:     "name_or_provides",
		//Architecture: "intel",
		//Official:     true,
	}
	log.Print(opts)
	q, _ := query.Values(opts)
	log.Print(q.Encode())
	req.URL.RawQuery = q.Encode()

	defaultClient := http.DefaultClient
	res, _ := defaultClient.Do(req)
	switch res.StatusCode {
	case http.StatusOK:
		var queryResult interface{}
		json.NewDecoder(res.Body).Decode(&queryResult)
		log.Print(queryResult)
	case http.StatusBadRequest:
		fallthrough
	case http.StatusUnauthorized:
		fallthrough
	case http.StatusNotFound:
		fallthrough
	case http.StatusConflict:
		fallthrough
	case http.StatusTooManyRequests:
		fallthrough
	case http.StatusInternalServerError:
		fallthrough
	default:
		log.Print(res.StatusCode)
	}

}
